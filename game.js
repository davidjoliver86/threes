BOARD_HEIGHT = 4;
BOARD_WIDTH = 4;

var Directions = {
	up: {'start': 0, 'end': BOARD_HEIGHT - 1, 'step': 1},
	left: {'start': 0, 'end': BOARD_WIDTH - 1, 'step': 1},
	down: {'start': BOARD_HEIGHT - 1, 'end': 0, 'step': -1},
	right: {'start': BOARD_WIDTH - 1, 'end': 0, 'step': -1},
}

var Board = {
	_board: [[0, 0, 0, 0],[0, 0, 0, 0],[0, 0, 0, 0],[0, 0, 0, 0]],

	getCleanBoard: function() {
		this._board = [[1, 1, 1, 1],[2, 2, 2, 2],[1, 1, 1, 1],[2,2,2,2]];
		return this;
	},

	getBoard: function() {
		return this._board.slice();
	},

	getRow: function(row) {
		return this._board[row];
	},

	getColumn: function(column) {
		var col = [];
		for (var row=0; row < this._board.length; row++) {
			col.push(this._board[row][column]);
		}
		return col;
	},

	getCell: function(row, column) {
		return this._board[row][column];
	},

	setRow: function(rowNum, rowArray) {
		this._board[rowNum] = rowArray;
	},

	setColumn: function(colNum, colArray) {
		for (var row=0; row < this._board.length; row++) {
			this._board[row][colNum] = colArray[row];
		};
	},

	move: function(fromR, fromC, toR, toC) {
		var fromValue = this._board[fromR][fromC];
		var toValue = this._board[toR][toC];
		var doMove = false;
		if ((fromValue == 1 && toValue == 2) || (fromValue == 2 && toValue == 1)) {
			doMove = true;
			var newValue = 3;
		}
		if ((fromValue == toValue) && toValue >= 3) {
			doMove = true;
			var newValue = 2 * toValue;
		}
		if (toValue == 0) {
			doMove = true;
			var newValue = fromValue;
		}
		if (doMove) {
			this._board[fromR][fromC] = 0;
			this._board[toR][toC] = newValue;
		}
	},

}

var Game = {
	isRunning: false,
	board: Board.getCleanBoard(),

	startGame: function() {},

	printBoard: function() {
		var html = "";
		for (var i=0; i<BOARD_HEIGHT; i++) {
			var rowhtml = "<div class='row'>";
			for (var j=0; j<BOARD_WIDTH; j++) {
				var value = this.board.getCell(i, j);
				rowhtml += "<div class='box num" + value + "'>" + this.board.getCell(i, j) + "</div>";
			}
			rowhtml += "</div>";
			html += rowhtml
		}
		$('.game').html(html);
	},

	insertNext: function(dir) {
		var randomValue, rowcol, rowcolidx, i, idx;
		
		randomValue = Math.floor(Math.random() * 3) + 1;

		if (dir === Directions.up || dir === Directions.down) {
			rowcolidx = dir === Directions.up ? BOARD_HEIGHT-1 : 0;
			rowcol = this.board.getRow(rowcolidx);
		};

		if (dir === Directions.left || dir === Directions.right) {
			rowcolidx = dir === Directions.left ? BOARD_WIDTH-1 : 0;
			rowcol = this.board.getColumn(rowcolidx);
		};

		// get a random index of a zero; pretend to fill in Math.random() and choose the lowest, then take the index of it
		for (i=0; i<rowcol.length; i++) {
			if (rowcol[i] === 0) rowcol[i] = Math.random();
		};

		idx = rowcol.indexOf(Math.min.apply(Math, rowcol));
		for (i=0; i<rowcol.length; i++) {
			if (i === idx) rowcol[i] = randomValue;
			if (0 < rowcol[i] && rowcol[i] < 1) rowcol[i] = 0;
		}

		if (dir === Directions.up || dir === Directions.down) {
			this.board.setRow(rowcolidx, rowcol);
		};

		if (dir === Directions.left || dir === Directions.right) {
			this.board.setColumn(rowcolidx, rowcol);;
		};
	},

	move: function(dir) {
		var startBoard = JSON.stringify(this.board.getBoard());

		if (dir == Directions.up || dir == Directions.down) { // columns are fixed 
			for (var col=0; col < BOARD_WIDTH; col++) {
				for (var i=dir['start']; i != dir['end']; i += dir['step']) {
					this.board.move(i+dir.step, col, i, col);
				};
			};
		};
		if (dir == Directions.left || dir == Directions.right) { // rows are fixed 
			for (var row=0; row < BOARD_HEIGHT; row++) {
				for (var i=dir['start']; i != dir['end']; i += dir['step']) {
					this.board.move(row, i+dir.step, row, i);
				};
			};
		};

		var endBoard = JSON.stringify(this.board.getBoard());
		console.log(startBoard[0] + ',' + startBoard[1] + ',' + startBoard[2]+ ',' + startBoard[3]);
		console.log(endBoard[0] + ',' + endBoard[1] + ',' + endBoard[2]+ ',' + endBoard[3]);
		if (startBoard != endBoard) this.insertNext(dir);
		this.printBoard();
	}
}