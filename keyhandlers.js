var KEY_UP = 38;
var KEY_DOWN = 40;
var KEY_LEFT = 37;
var KEY_RIGHT = 39;

$("body").keydown(function(event) {
	event.preventDefault();
	if (event.which == KEY_UP) Game.move(Directions.up);
	if (event.which == KEY_DOWN) Game.move(Directions.down);
	if (event.which == KEY_LEFT) Game.move(Directions.left);
	if (event.which == KEY_RIGHT) Game.move(Directions.right);
});